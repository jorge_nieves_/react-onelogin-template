import {createSlice, nanoid} from "@reduxjs/toolkit";

const initialState = {
    accessToken: null,
    idToken: null,
    state: null,
    nonce: null
};

export const tokenSlice = createSlice({
    name: 'token',
    initialState,
    reducers: {
        receiveAccessToken: (state, action) => {
            return {
                ...state,
                accessToken: action.payload,
            }
        },
        receiveIdToken: (state, action) => {
            return {
                ...state,
                idToken: action.payload,
            }
        },
        generateStateAndNonce: (state) => {
            return {
                ...state,
                state: state.state || nanoid(32),
                nonce: state.nonce || nanoid(32),
            }
        },
        clearTokens: () => {
            return initialState;
        },
    },
});

export const {receiveAccessToken, receiveIdToken, generateStateAndNonce, clearTokens} = tokenSlice.actions;

export default tokenSlice.reducer;
