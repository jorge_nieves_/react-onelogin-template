import {createSlice} from "@reduxjs/toolkit";

const initialState = null;

export const profileSlice = createSlice({
    name: 'profile',
    initialState,
    reducers: {
        receiveProfileData: (state, action) => {
            return {
                email: action.payload.email,
                fullname: action.payload.name,
                username: action.payload.preferred_username,
                company: action.payload.company,
                dept: action.payload.department,
                title: action.payload.title
            };
        },
        clearProfile: () => {
            return initialState;
        },
    },
});

export const {receiveProfileData, clearProfile} = profileSlice.actions;

export default profileSlice.reducer;
