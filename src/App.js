import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import './App.css';
import {useSelector} from "react-redux";
import ProtectedRoute from "./components/ProtectedRoute";
import Header from "./components/Header";
import Login from "./components/Login";
import AuthPage from "./components/AuthPage";
import {Dashboard} from "./components/Dashboard";

const App = () => {
    const loggedIn = useSelector(state => state.token.accessToken);
    return (
        <Router>
            <Header />
            <div className="App">
                <Switch>
                    <ProtectedRoute
                        component={Login}
                        isLoggedIn={!loggedIn}
                        path="/login"
                        redirect="/dashboard"
                    />
                    <ProtectedRoute
                        component={Dashboard}
                        isLoggedIn={loggedIn}
                        path="/dashboard"
                    />
                    <Route
                        component={AuthPage}
                        path="/auth"
                    />
                </Switch>
            </div>
        </Router>
    )
};

export default App;
