import React from "react";
import {Route, Redirect} from "react-router-dom";

// eslint-disable-next-line react/prop-types
const ProtectedRoute = ({component: Component, isLoggedIn, redirect, ...rest}) => {
    return (
        <Route {...rest}
               render={(routeProps) => (
                   isLoggedIn
                       ? (<Component {...routeProps} />)
                       : (<Redirect to={redirect}/>)
               )}/>
    );
};

export default ProtectedRoute;
