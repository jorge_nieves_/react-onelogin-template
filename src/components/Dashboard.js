import React from "react";
import {useSelector} from "react-redux";

export const Dashboard = () => {
    const profile = useSelector(state => state.profile);
    return (
        <div className="App-dashboard">
            <div>Name: {profile.fullname}</div>
            <div>User: {profile.username}</div>
            <div>Email: {profile.email}</div>
            <div>Company: {profile.company}</div>
            <div>Dept: {profile.dept}</div>
            <div>Title: {profile.title}</div>
        </div>
    )
}
