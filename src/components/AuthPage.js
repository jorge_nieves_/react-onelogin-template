import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import jwtDecode from "jwt-decode";
import {isEmpty} from "lodash";
import DotsLoader from "./DotsLoader";
import {parse} from "query-string";
import {receiveProfileData} from "../features/auth/profileSlice";
import {receiveAccessToken, receiveIdToken} from "../features/auth/tokenSlice";

const AuthPage = () => {
    const dispatch = useDispatch();
    const profile = useSelector((state) => state.profile);
    if (isEmpty(profile)) {
        const hash = window.location.hash;
        const response = parse(hash);
        if (response.error) {
            console.log(response.error);
            alert(response.error_description);
            return <Redirect to="/"/>
        } else {
            dispatch(receiveAccessToken(response.access_token));
            dispatch(receiveIdToken(response.idToken));
            dispatch(receiveProfileData(jwtDecode(response.id_token)));
            console.log(jwtDecode(response.id_token))
        }
        return <DotsLoader/>
    } else {
        return <Redirect to="/dashboard"/>
    }
};

export default AuthPage;
