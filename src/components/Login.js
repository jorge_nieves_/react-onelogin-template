import React from 'react'
import DotsLoader from "./DotsLoader";
import {useDispatch, useSelector} from "react-redux";
import * as oidcApi from "../api/oidcApi"
import {generateStateAndNonce} from "../features/auth/tokenSlice"

export const Login = () => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.token.state);
    const nonce = useSelector(state => state.token.nonce);

    if (state === null || nonce === null) {
        dispatch(generateStateAndNonce())
    } else {
        oidcApi.beginAuth({state, nonce});
    }

    return (
        <DotsLoader/>
    );
};

export default Login;
