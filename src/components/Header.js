import React from "react";
import {useDispatch} from "react-redux";
import "../css/Header.css"
import {clearTokens} from "../features/auth/tokenSlice";
import {clearProfile} from "../features/auth/profileSlice";

const Header = () => {
    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(clearTokens());
        dispatch(clearProfile());
    }
    return (
        <div>
            <header className="App-header">
                <h2>Client</h2>
                <div>
                    <button onClick={handleLogout}>Logout</button>
                </div>
            </header>
        </div>
    );
};

export default Header;
