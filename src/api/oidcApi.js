import {stringify} from "query-string";

const authority = "https://jn-dev.onelogin.com/oidc/2";
const client_id = "5ce61ee0-f30e-0139-644e-0a03d6f3dab5196460";
const redirect_uri = "http://localhost:3000/auth";
const response_type = "id_token token";
const scope = "openid profile";

export const beginAuth = ({state, nonce}) => {
    const params = stringify({
        client_id,
        redirect_uri,
        response_type,
        scope,
        state,
        nonce
    });
    const authUrl = `${authority}/auth?${params}`;
    console.log(`Auth URL: ${authUrl}`);

    window.location.assign(authUrl);
}
