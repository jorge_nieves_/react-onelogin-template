import { configureStore } from '@reduxjs/toolkit';
import tokenReducer from '../features/auth/tokenSlice'
import profileReducer from '../features/auth/profileSlice'

const store = configureStore({
    reducer: {
        token: tokenReducer,
        profile: profileReducer,
    },
});

export default store;
